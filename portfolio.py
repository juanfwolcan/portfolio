class Money:
    def __init__(self, amount, currency) -> None:
        self._amount = amount
        self._currency = currency
    
    def __eq__(self, other) -> bool:
        return type(self) == type(other) and self._currency == other._currency and self._amount == other._amount
    
    def __hash__(self) -> int:
        return hash(self._amount)
    
    def times(self, multiplier):
        return Money(amount = self._amount * multiplier, currency = self._currency)
    
    def dollar(amount):
        return Money(amount, 'USD')
    
    def franc(amount):
        return Money(amount, 'CHF')   
    
    
    
