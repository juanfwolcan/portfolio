def test_multiplication():
    #Test that you can multiply a Dollar for a given amount
    from portfolio import Money
    five = Money.dollar(amount = 5)
    assert five.times(multiplier=2) == Money.dollar(10)
    assert five.times(3) == Money.dollar(15)
    

def test_equality():
    from portfolio import Money
    assert Money.dollar(amount = 5) == Money.dollar(amount = 5)
    assert Money.dollar(amount = 5) != Money.dollar(amount = 6)
    assert Money.dollar(amount = 5) != None
    assert Money.franc(amount = 5) == Money.franc(amount = 5)
    assert Money.franc(amount = 5) != Money.franc(amount = 6)
    assert Money.franc(amount = 5) != None
    assert Money.franc(amount = 5) != Money.dollar(amount = 5)
    
    
def test_hashable():
    from portfolio import Money
    assert hash(Money.dollar(amount = 5)) == hash(Money.dollar(amount = 5))
    
def test_franc_multiplication():
    # test that you can multiply a Dollar by a number and get the right amount.
    from portfolio import Money
    five = Money.franc(5)
    assert Money.franc(10) == five.times(2)
    assert Money.franc(15) == five.times(3)
    
def test_currency():
    from portfolio import Money
    assert Money.dollar(1)._currency == 'USD'
    assert Money.franc(1)._currency == 'CHF'